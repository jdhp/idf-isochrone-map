=================
IDF Isochrone Map
=================

Copyright (c) 2019 Jérémie DECOCK (www.jdhp.org)

* Web site: http://www.jdhp.org/software_en.html#idfim
* Online documentation: https://jdhp.gitlab.io/idfim
* Examples: https://jdhp.gitlab.io/idfim/gallery/

* Notebooks: https://gitlab.com/jdhp/idf-isochrone-map-notebooks
* Source code: https://gitlab.com/jdhp/idf-isochrone-map
* Issue tracker: https://gitlab.com/jdhp/idf-isochrone-map/issues
* IDF Isochrone Map on PyPI: https://pypi.org/project/idfim
* IDF Isochrone Map on Anaconda Cloud: https://anaconda.org/jdhp/idfim


Description
===========

Make an isochrone map of Paris region using public transportation.

Morning (5am / 11am) :
- Starting points: addresses registered in the configuration file
- Ending points: all train stations and bus stations

Evening (4pm / 8pm) :
- Starting points: all train stations and bus stations
- Ending points: addresses registered in the configuration file

On ne cible que les gares / arrêts de bus car les trajets à pied
pour relier n'importe quel autre point sont déterministes en temps
et peuvent être calculés après coup ou à la volée.

APIs:
- iledefrance-mobilites (APIs)
  - https://portal.api.iledefrance-mobilites.fr/fr/
    - Calculateur Vianavigo - Accès générique (Navitia) 1.0.0 https://portal.api.iledefrance-mobilites.fr/fr/?option=com_apiportal&view=apitester&usage=api&tab=tests&apiName=Calculateur%20Vianavigo%20-%20Acc%C3%A8s%20g%C3%A9n%C3%A9rique%20%28Navitia%29&apiId=a6d18ce6-ef1a-4faf-8545-8b0c8b1669c6&menuId=174&managerId=1&renderTool=2&type=rest&apiVersion=1.0.0
    - Recherche d'itinéraires 1.0.0 https://portal.api.iledefrance-mobilites.fr/fr/?option=com_apiportal&view=apitester&usage=api&tab=tests&apiName=Recherche%20d%27itin%C3%A9raires&apiId=7e8f5b2a-985b-41de-b3aa-1b1c26f087d4&menuId=174&managerId=1&renderTool=2&type=rest&apiVersion=1.0.0
    - Calcul d'isochrones 1.0.0 https://portal.api.iledefrance-mobilites.fr/fr/?option=com_apiportal&view=apitester&usage=api&tab=tests&apiName=Calcul%20d%27isochrones&apiId=435c46be-51da-4f24-a368-12fed143fc65&menuId=174&managerId=1&renderTool=2&type=rest&apiVersion=1.0.0
  - Documentation:
    - Documentation IDF-mobilités: https://portal.api.iledefrance-mobilites.fr/images/com_apiportal/doc/IDFM-portailAPI-documentation-1_1.pdf
    - Documentation technique sur Navitia: https://doc.navitia.io/
    - OAuth: https://zestedesavoir.com/articles/1616/comprendre-oauth-2-0-par-lexemple/
- iledefrance-mobilites (Open Data)
  - https://data.iledefrance-mobilites.fr/pages/home/
- geoportail : https://www.geoportail.gouv.fr/actualites/service-de-calcul-disochrones-et-disodistances
- Google maps for cars / motor bikes / bikes / walk

Note:

    This project is still in beta stage, so the API is not finalized yet.


Dependencies
============

*  Python >= 3.0

.. _install:

Installation
============

Gnu/Linux
---------

You can install, upgrade, uninstall IDF Isochrone Map with these commands (in a
terminal)::

    pip install --pre idfim
    pip install --upgrade idfim
    pip uninstall idfim

Or, if you have downloaded the IDF Isochrone Map source code::

    python3 setup.py install

.. There's also a package for Debian/Ubuntu::
.. 
..     sudo apt-get install idfim

Windows
-------

.. Note:
.. 
..     The following installation procedure has been tested to work with Python
..     3.4 under Windows 7.
..     It should also work with recent Windows systems.

You can install, upgrade, uninstall IDF Isochrone Map with these commands (in a
`command prompt`_)::

    py -m pip install --pre idfim
    py -m pip install --upgrade idfim
    py -m pip uninstall idfim

Or, if you have downloaded the IDF Isochrone Map source code::

    py setup.py install

MacOSX
-------

.. Note:
.. 
..     The following installation procedure has been tested to work with Python
..     3.5 under MacOSX 10.9 (*Mavericks*).
..     It should also work with recent MacOSX systems.

You can install, upgrade, uninstall IDF Isochrone Map with these commands (in a
terminal)::

    pip install --pre idfim
    pip install --upgrade idfim
    pip uninstall idfim

Or, if you have downloaded the IDF Isochrone Map source code::

    python3 setup.py install


Documentation
=============

* Online documentation: https://jdhp.gitlab.io/idfim
* API documentation: https://jdhp.gitlab.io/idfim/api.html


Example usage
=============

TODO


Bug reports
===========

To search for bugs or report them, please use the IDF Isochrone Map Bug Tracker at:

    https://gitlab.com/jdhp/idf-isochrone-map/issues


License
=======

This project is provided under the terms and conditions of the `MIT License`_.


.. _MIT License: http://opensource.org/licenses/MIT
.. _command prompt: https://en.wikipedia.org/wiki/Cmd.exe
