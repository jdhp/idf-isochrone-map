=================
IDF Isochrone Map
=================

Make an isochrone map of Paris region using public transportation.

Note:

    This project is in beta stage, the API may change.

Contents:

.. toctree::
   :maxdepth: 2

   intro
   API <api>
   developer

* Web site: http://www.jdhp.org/software_en.html#idfim
* Online documentation: https://jdhp.gitlab.io/idfim
* Source code: https://gitlab.com/jdhp/idf-isochrone-map
* Issue tracker: https://gitlab.com/jdhp/idf-isochrone-map/issues
* idfim on PyPI: https://pypi.org/project/idfim

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Credits
=======

.. include:: ../AUTHORS
   :literal:

License
=======

.. highlight:: none

.. include:: ../LICENSE
   :literal:

