import json
import tempfile
import geopandas as gpd
import idfim.io.geojson

def polygon_to_geopandas_dataframe(polygon):

    with tempfile.NamedTemporaryFile(suffix=".json") as tf:
        geojson_file_path = tf.name

        idfim.io.geojson.polygon_to_geojson_file(polygon, geojson_file_path)

        df = gpd.read_file(geojson_file_path)

    return df


def geopandas_dataframe_to_polygon(df):

    with tempfile.NamedTemporaryFile(suffix=".geojson") as tf:
        geojson_file_path = tf.name

        df.to_file(geojson_file_path, driver='GeoJSON')

        # TODO...
        with open(geojson_file_path , "r") as fd:
            geojson = json.load(fd)

        polygon = geojson["features"][0]["geometry"]

    return polygon


def polygon_intersection(polygon_list):
    if len(polygon_list) > 0:
        df = polygon_to_geopandas_dataframe(polygon_list[0])

        for polygon in polygon_list[1:]:
            df2 = polygon_to_geopandas_dataframe(polygon)
            df = df.intersection(df2)

        return geopandas_dataframe_to_polygon(df)
    else:
        return None


def polygon_union(polygon_list):
    if len(polygon_list) > 0:
        df = polygon_to_geopandas_dataframe(polygon_list[0])

        for polygon in polygon_list[1:]:
            df2 = polygon_to_geopandas_dataframe(polygon)
            df = df.union(df2)

        return geopandas_dataframe_to_polygon(df)
    else:
        return None
