import datetime
import requests
import time
import os

def get_isochrone_polygon(api_token, starting_point, max_duration, datetime_str=None):
    '''
    starting_point_str:
    str gps position
    
    datetime_str:
    str 20191104T080000',    # AAAAMMJJTHHMMSS
    default: now
    
    max_duration
    int
    minutes
    '''
    #Paramètres de la requête (URL?)
    url = 'https://traffic.api.iledefrance-mobilites.fr/v1/mri-isochrones/isochrones'

    starting_point_lat_long_tuple = starting_point.split(",")
    starting_point_long_lat_str = starting_point_lat_long_tuple[1] + ";" + starting_point_lat_long_tuple[0]

    params = {
        'from': starting_point_long_lat_str,    # {long;lat}. Ex : '2.335547;48.876076'
        'datetime': datetime_str,               # Format: YYYYMMDDTHHMMSS
        'max_duration': str(max_duration * 60)  # In seconds. Ex: 1800
    }

    headers = {
        'Accept-Encoding': 'gzip',
        'Authorization': 'Bearer ' + api_token
    }

    response = requests.get(url, params=params, headers=headers)

    if response.status_code != 200:
        raise Exception('Status:', response.status_code, 'Erreur sur la requête; fin de programme')

    response_dict = response.json()
    
    polygon_list = [isochrone['geojson'] for isochrone in response_dict['isochrones']]
    
    return polygon_list



def get_isochrone_polygon_robust(api_token, starting_point, max_duration, datetime_str, attempts=3, wait_on_error=5, log_path=None):
    isochrone_polygon = None

    while attempts > 0:
        try:
            isochrone_polygon = get_isochrone_polygon(api_token=api_token,
                                                      starting_point=starting_point,
                                                      max_duration=max_duration,
                                                      datetime_str=datetime_str)
            attempts = 0
        except Exception as e:
            attempts -= 1
            print(e)

            if log_path is not None:
                log_path = os.path.expanduser(log_path)  # to handle "~/..." paths
                log_path = os.path.abspath(log_path)     # to handle relative paths

                with open(log_path, 'a') as fd:
                    print(datetime.datetime.now().isoformat(), starting_point, datetime_str, e, file=fd)

            print("{} remaining attempts".format(attempts))
            time.sleep(wait_on_error)  # Wait a bit

    return isochrone_polygon
