import yaml
import json
import os

def load_config(config_path):

    config_path = os.path.expanduser(config_path)  # to handle "~/..." paths
    config_path = os.path.abspath(config_path)     # to handle relative paths

    if config_path.lower().endswith(".yaml"):
        with open(config_path, "r") as stream:
            config_dict = yaml.safe_load(stream)
    elif config_path.lower().endswith(".json"):
        with open(config_path, "r") as fd:
            config_dict = json.load(fd)
    else:
        raise ValueError("Unknown config file format: " + config_path)

    return config_dict
