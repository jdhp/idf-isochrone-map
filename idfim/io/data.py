"""This module provides functions to save and load the JSON database
where all downloaded isochron maps are stored."""

import json
import lzma
import os

def load_data(data_path):
    """Load the JSON database.

    Parameters
    ----------
    data_path : str
        The path of the JSON database.

    Returns
    -------
    dict
        The JSON dictionary.
    """

    data_path = os.path.expanduser(data_path)  # to handle "~/..." paths
    data_path = os.path.abspath(data_path)     # to handle relative paths

    if os.path.exists(data_path):
        if not os.path.isfile(data_path):
            raise ValueError(data_path + " is not a file")

        if data_path.lower().endswith(".json"):
            with open(data_path, "r") as fd:
                data_dict = json.load(fd)
        elif data_path.lower().endswith(".json.xz"):
            with lzma.open(data_path, "rt") as fd:
                data_dict = json.loads(fd.read())
        else:
            raise ValueError("Unknown data file format: " + data_path)
    else:
        data_dict = None

    return data_dict


def save_data(data, data_path):
    """Save the JSON database.

    Parameters
    ----------
    data : dict
        The JSON dictionary to save.
    data_path : str
        The path of the JSON database.

    Returns
    -------
    dict
        The JSON dictionnary.
    """

    data_path = os.path.expanduser(data_path)  # to handle "~/..." paths
    data_path = os.path.abspath(data_path)     # to handle relative paths

    if data_path.lower().endswith(".json"):
        with open(data_path, "w") as fd:
            # https://stackoverflow.com/questions/16311562/python-json-without-whitespaces
            # https://docs.python.org/3/library/json.html#json.dump
            json.dump(data, fd, separators=(',', ':'))
            #json.dump(data, fd)
            #json.dump(data, fd, sort_keys=True, indent=2)  # Pretty print format
    else:
        raise ValueError("Unknown data file format: " + data_path)
