import json
import os


def polygon_to_geojson_file(polygon, geojson_file_path):

    geojson_file_path = os.path.expanduser(geojson_file_path)  # to handle "~/..." paths
    geojson_file_path = os.path.abspath(geojson_file_path)     # to handle relative paths

    if geojson_file_path.lower().endswith(".json"):

        geojson_data = {
            "features": [
                {
                    "geometry": polygon,
                    "type": "Feature"
                }
            ],
            "type": "FeatureCollection"
        }

        with open(geojson_file_path, "w") as fd:
            json.dump(geojson_data, fd)
            #json.dump(geojson_data, fd, sort_keys=True, indent=4)
    else:
        raise ValueError("Unknown data file format: " + geojson_file_path)
