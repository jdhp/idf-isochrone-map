#!/usr/bin/env python3

# -*- coding: utf-8 -*-

# Copyright (c) 2019 Jérémie DECOCK (http://www.jdhp.org)

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
Convert data files to HTML
"""

import argparse
import os
import sys

import idfim.commands
import idfim.dataframe
import idfim.io.data

HTML_TEMPLATE = """<!DOCTYPE html>
<html>
    <head>
        <title>Isochrone Map</title>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>

        <script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
        <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>

        <meta name="author" content="Jérémie DECOCK" />
        <meta name="copyright" content="copyright (c) 2019 Jérémie DECOCK" />

        <link rel="stylesheet" type="text/css" href="idfim.css" media="screen" title="Normal" />
    </head>
    <body>
        <h1>{h1}</h1>

        <div id="map" style="width: 1000px; height: 600px;"></div>

        <p>
        {desc}
        </p>

        <script>
            var starting_point = [{starting_point}];
            var starting_point_label = "{starting_point_label}";
            var destination_points = {destination_points};
            var destination_points_label = {destination_points_label};
            var geojson_polygon_list = {geojson_polygon_list};

            // Create a drawable map using Leaflet
            var map = L.map('map');

            L.tileLayer('https://api.tiles.mapbox.com/v4/{{id}}/{{z}}/{{x}}/{{y}}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {{
                maxZoom: 18,
                attribution: '<a href="https://gitlab.com/jdhp/idf-isochrone-map">idf-isochrone-map</a> © <a href="http://www.jdhp.org">Jérémie Decock</a> | ' +
                    'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                id: 'mapbox.streets'
            }}).addTo(map);

            // Custom markers: https://leafletjs.com/examples/custom-icons/
            var grayMarker = L.icon({{
                iconUrl: 'marker-icon-gray.png',
                shadowUrl: 'marker-shadow.png',

                iconSize:     [25, 41], // size of the icon
                shadowSize:   [41, 41], // size of the shadow
                iconAnchor:   [12, 41], // point of the icon which will correspond to marker's location
                shadowAnchor: [12, 41], // the same for the shadow
                popupAnchor:  [ 1,-34]  // point from which the popup should open relative to the iconAnchor
            }});

            // Add marker for each destination
            for(var destination_point_index in destination_points) {{
                L.marker(destination_points[destination_point_index], {{icon: grayMarker}}).addTo(map)
                    .bindPopup(destination_points_label[destination_point_index]);
            }}

            L.marker(starting_point).addTo(map)
                .bindPopup(starting_point_label);

            $.each(geojson_polygon_list, function(i, polygon) {{
              var myLayer = L.geoJson(polygon).addTo(map);
              var newBounds = myLayer.getBounds();
              map.fitBounds(newBounds);
            }});
        </script>
    </body>
</html>
"""


def polygon_to_html(isochrone_polygon, html_file_path, starting_point="", starting_point_label="", destination_points=[], destination_points_label=[], date_str=None, time_str=None, duration=None):
    print("write to", html_file_path)

    with open(html_file_path, "w") as fd:
        destination_points = "[[" + "],[".join(destination_points) + "]]"
        destination_points_label = '["' + '", "'.join(destination_points_label) + '"]'
        geojson_polygon_list = str(isochrone_polygon)

        h1 = "Carte isochrone du {date} à {heure} (trajet de {duree} minutes)".format(date=date_str, heure=time_str, duree=duration)   # TODO

        desc = "Zones atteignables en moins de {duree} minutes <b>via les transports en communs (RER, métro, tramway, bus et marche à pied)</b> en partant de la coordonnée GPS {gps} ({label}) le {date} à {heure}.".format(duree=duration, gps=str(starting_point), label=starting_point_label, date=date_str, heure=time_str)   # TODO

        html = HTML_TEMPLATE.format(h1=h1,
                                    desc=desc,
                                    starting_point=starting_point,
                                    starting_point_label=starting_point_label,
                                    destination_points=destination_points,
                                    destination_points_label=destination_points_label,
                                    geojson_polygon_list=geojson_polygon_list)

        print(html, file=fd)


def data_to_html(data, html_dir_path, config_dict):

    html_dir_path = os.path.expanduser(html_dir_path)  # to handle "~/..." paths
    html_dir_path = os.path.abspath(html_dir_path)     # to handle relative paths

    if not os.path.exists(html_dir_path):
        print(html_dir_path, "does not exist")
        # Recursive directory-creation function (makes all intermediate-level directories needed to contain the leaf directory)
        os.makedirs(html_dir_path)

    if not os.path.isdir(html_dir_path):
        raise Exception(html_dir_path + " is not a directory ; cannot write HTML files.")

    starting_point_list = []
    if 'domiciles' in data:
        starting_point_list = list(data['domiciles'].items())
    if 'workplaces' in data:
        starting_point_list += list(data['workplaces'].items())

    for starting_point, d1 in starting_point_list:
        for date_str, d2 in ((d1_key, d1_value) for d1_key, d1_value in d1.items() if d1_key not in ("address", "label")):
            for time_str, d3 in d2.items():
                for duration_str, isochrone_polygon in d3.items():
                    hour, minute = time_str.split(":")
                    year, month, day = date_str.split("-")
                    starting_point_label = d1["label"]
                    starting_point_address = d1["address"]

                    # TODO: improve the following 6 lines...
                    if ('domiciles' in data) and (starting_point in data['domiciles']):
                        destination_points = config_dict['workplaces'].keys()
                        destination_points_label = [v["label"] for v in config_dict['workplaces'].values()]
                    elif ('workplaces' in data) and (starting_point in data['workplaces']):
                        destination_points = config_dict['domiciles'].keys()
                        destination_points_label = [v["label"] for v in config_dict['domiciles'].values()]

                    html_file = "{}_{}-{}-{}_{}h{}_{}mn.html".format(starting_point_label.lower().replace(" ", "_").replace("-", "_"),
                                                                     year,
                                                                     month,
                                                                     day,
                                                                     hour,
                                                                     minute,
                                                                     duration_str)
                    html_file_path = os.path.join(html_dir_path, html_file)

                    polygon_to_html(isochrone_polygon, html_file_path, starting_point, starting_point_label, destination_points, destination_points_label, date_str, time_str, duration_str)


def union_polygons_to_html(data, html_dir_path, config_dict):
    html_dir_path = os.path.expanduser(html_dir_path)  # to handle "~/..." paths
    html_dir_path = os.path.abspath(html_dir_path)     # to handle relative paths

    if not os.path.exists(html_dir_path):
        print(html_dir_path, "does not exist")
        # Recursive directory-creation function (makes all intermediate-level directories needed to contain the leaf directory)
        os.makedirs(html_dir_path)

    if not os.path.isdir(html_dir_path):
        raise Exception(html_dir_path + " is not a directory ; cannot write HTML files.")

    # UNION OVER TIME #########################################################

    starting_point_list = []
    if 'domiciles' in data:
        starting_point_list = list(data['domiciles'].items())
    if 'workplaces' in data:
        starting_point_list += list(data['workplaces'].items())

    d = {}
    for starting_point, d1 in starting_point_list:
        for date_str, d2 in ((d1_key, d1_value) for d1_key, d1_value in d1.items() if d1_key not in ("address", "label")):
            for time_str, d3 in d2.items():
                for duration_str, isochrone_polygon in d3.items():
                    if not (starting_point, date_str, duration_str) in d: 
                        d[(starting_point, date_str, duration_str)] = []

                    d[(starting_point, date_str, duration_str)].append(isochrone_polygon[0])

    d2 = {}
    for k, polygon_list in d.items():
        p = idfim.dataframe.polygon_union(polygon_list)

        if ('domiciles' in data) and (k[0] in data['domiciles']):
            d2[k] = p

            starting_point_label = data['domiciles'][k[0]]["label"]
            starting_point_address = data['domiciles'][k[0]]["address"]

            html_file = "union_{}_{}_{}.html".format(data['domiciles'][k[0]]['label'].lower().replace(" ", "_").replace("-", "_"),
                                                     k[1],
                                                     k[2])
        elif ('workplaces' in data) and (k[0] in data['workplaces']):
            d2[k] = p

            starting_point_label = data['workplaces'][k[0]]["label"]
            starting_point_address = data['workplaces'][k[0]]["address"]

            html_file = "union_{}_{}_{}.html".format(data['workplaces'][k[0]]['label'].lower().replace(" ", "_").replace("-", "_"),
                                                     k[1],
                                                     k[2])

        html_file_path = os.path.join(html_dir_path, html_file)

        # TODO: improve the following 6 lines...
        if ('domiciles' in data) and (starting_point in data['domiciles']):
            destination_points = config_dict['workplaces'].keys()
            destination_points_label = [v["label"] for v in config_dict['workplaces'].values()]
        elif ('workplaces' in data) and (starting_point in data['workplaces']):
            destination_points = config_dict['domiciles'].keys()
            destination_points_label = [v["label"] for v in config_dict['domiciles'].values()]

        polygon_to_html([d2[k]], html_file_path, starting_point, starting_point_label, destination_points, destination_points_label)

    return d2


def intersect_polygons_to_html(union_data, html_dir_path, config_dict):
    html_dir_path = os.path.expanduser(html_dir_path)  # to handle "~/..." paths
    html_dir_path = os.path.abspath(html_dir_path)     # to handle relative paths

    if not os.path.exists(html_dir_path):
        print(html_dir_path, "does not exist")
        # Recursive directory-creation function (makes all intermediate-level directories needed to contain the leaf directory)
        os.makedirs(html_dir_path)

    if not os.path.isdir(html_dir_path):
        raise Exception(html_dir_path + " is not a directory ; cannot write HTML files.")

    # INTERSECTION OVER DOMICILES #############################################

    d3 = {}
    for k, isochrone_polygon in union_data.items():
        starting_point = k[0]
        date_str = k[1]
        duration_str = k[2]

        if ('domiciles' in config_dict) and (starting_point in config_dict['domiciles']):
            starting_point_label = config_dict['domiciles'][starting_point]["label"]
        elif ('workplaces' in config_dict) and (starting_point in config_dict['workplaces']):
            starting_point_label = config_dict['workplaces'][starting_point]["label"]
        else:
            starting_point_label = ""

        if not (date_str, duration_str) in d3: 
            d3[(date_str, duration_str)] = []

        d3[(date_str, duration_str)].append(isochrone_polygon)

    starting_points = config_dict['domiciles'].keys()
    starting_points_label = [v["label"] for v in config_dict['domiciles'].values()]
    destination_points = config_dict['workplaces'].keys()
    destination_points_label = [v["label"] for v in config_dict['workplaces'].values()]

    for k, polygon_list in d3.items():
        p = idfim.dataframe.polygon_intersection(polygon_list)

        html_file = "intersection_{}_{}.html".format(k[0], k[1])
        html_file_path = os.path.join(html_dir_path, html_file)

        polygon_to_html([p], html_file_path, starting_point, starting_point_label, destination_points, destination_points_label)


def data_file_to_html(data_file_path, html_dir_path, config_dict):
    data_file_path = os.path.expanduser(data_file_path)  # to handle "~/..." paths
    data_file_path = os.path.abspath(data_file_path)     # to handle relative paths

    #union_data = {}

    if os.path.isfile(data_file_path) and data_file_path.lower().endswith((".json", ".json.xz")):
        print("load", data_file_path)
        data = idfim.io.data.load_data(data_path=data_file_path)

        data_to_html(data, html_dir_path=html_dir_path, config_dict=config_dict)
        #_union_data = union_polygons_to_html(data, html_dir_path=html_dir_path, config_dict=config_dict)
        #union_data.update(_union_data)
    else:
        raise Exception('Wrong file extension: {}. Expect a ".json" or ".json.xz" file.'.format(data_file_path))

    #intersect_polygons_to_html(union_data, html_dir_path=html_dir_path, config_dict=config_dict)


def data_dir_to_html(data_dir_path, html_dir_path, config_dict):
    data_dir_path = os.path.expanduser(data_dir_path)  # to handle "~/..." paths
    data_dir_path = os.path.abspath(data_dir_path)     # to handle relative paths

    for file_name in os.listdir(data_dir_path):
        file_path = os.path.join(data_dir_path, file_name)
        if os.path.isfile(file_path) and file_name.lower().endswith((".json", ".json.xz")):
            data_file_to_html(file_path, html_dir_path, config_dict)


def make_index(html_dir_path):
    html_dir_path = os.path.expanduser(html_dir_path)  # to handle "~/..." paths
    html_dir_path = os.path.abspath(html_dir_path)     # to handle relative paths

    position_dict = {}

    for file_name in os.listdir(html_dir_path):
        file_path = os.path.join(html_dir_path, file_name)

        if os.path.isfile(file_path) and file_name.lower().endswith(".html"):
            base_name = file_name.split(".")[0]
            try:
                (position, date, time, duration) = base_name.split("_")

                if position not in position_dict:
                    position_dict[position] = {}

                if date not in position_dict[position]:
                    position_dict[position][date] = []

                position_dict[position][date].append('<a href="{file_name}">{time} ({duration})</a>'.format(time=time, duration=duration, file_name=file_name))
            except:
                pass

    with open(os.path.join(html_dir_path, "index.html"), "w") as fd:
        print('<html>', file=fd)
        print('<body>', file=fd)
        print('<h1>Isochrone maps</h1>', file=fd)

        for position in position_dict:
            print('<h2>{}</h2>'.format(position), file=fd)
            for date in sorted(position_dict[position]):
                print('<h3>{}</h3>'.format(date), file=fd)
                for link in sorted(position_dict[position][date]):
                    print(link + "<br>", file=fd)

        print('</body>', file=fd)
        print('</html>', file=fd)


def main():
    """Main function"""

    # Parse arguments ###############################################

    parser = argparse.ArgumentParser(description='Make an isochrone map of Paris region using public transportation.')

    parser.add_argument("--config", "-c", default=idfim.commands.DEFAULT_CONFIG_PATH, metavar="STRING",
            help="Configuration file path [default: {}]".format(idfim.commands.DEFAULT_CONFIG_PATH))

    parser.add_argument("--html", "-o", default=idfim.commands.DEFAULT_HTML_PATH, metavar="STRING",
            help="Output html path [default: {}]".format(idfim.commands.DEFAULT_HTML_PATH))

    parser.add_argument("fileargs", nargs="+", metavar="FILES",
            help="The data files (or directories) to process")

    args = parser.parse_args()

    config_path = args.config
    html_path = args.html
    data_paths = args.fileargs

    # Get config params #############################################

    config_dict = idfim.io.config.load_config(config_path=config_path)

    # Make HTML files ###############################################
    
    for data_path in data_paths:
        if os.path.isfile(data_path):
            data_file_to_html(data_path, html_path, config_dict)
        elif os.path.isdir(data_path):
            data_dir_to_html(data_path, html_path, config_dict)
        else:
            raise Exception("The data path have to be a file or a directory:", data_path)

    make_index(html_path)


if __name__ == '__main__':
    main()
