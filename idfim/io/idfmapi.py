import json
import os
import requests

'''
API documentation:
- http://doc.navitia.io/#isochrones
- https://portal.api.iledefrance-mobilites.fr/fr/?option=com_apiportal&view=apitester&usage=api&tab=tests&managerId=1&apiName=Calcul%20d%27isochrones&apiId=435c46be-51da-4f24-a368-12fed143fc65&tags=aXNvY2hyb25lcyxpc29jaHJvbmVz&apiVersion=1.0.0&type=rest&menuId=174
- https://jsfiddle.net/kisiodigital/u22zsg9y/
- http://doc.navitia.io/#isochrones-currently-in-beta
'''

DEFAULT_IDFM_CREDENTIALS_PATH = "~/.idfm_api.json"


def load_idfm_credentials(idfm_credentials_path=DEFAULT_IDFM_CREDENTIALS_PATH):

    idfm_credentials_path = os.path.expanduser(idfm_credentials_path)  # to handle "~/..." paths
    idfm_credentials_path = os.path.abspath(idfm_credentials_path)     # to handle relative paths

    with open(idfm_credentials_path, "r") as fd:
        idfm_credentials_dict = json.load(fd)

    return idfm_credentials_dict


def load_idfm_unitary_app_credentials(idfm_credentials_path=DEFAULT_IDFM_CREDENTIALS_PATH):
    idfm_credentials_dict = load_idfm_credentials(idfm_credentials_path=DEFAULT_IDFM_CREDENTIALS_PATH)
    return idfm_credentials_dict["unitary_app"]["id"], idfm_credentials_dict["unitary_app"]["key"]


def load_idfm_global_app_credentials(idfm_credentials_path=DEFAULT_IDFM_CREDENTIALS_PATH):
    idfm_credentials_dict = load_idfm_credentials(idfm_credentials_path=DEFAULT_IDFM_CREDENTIALS_PATH)
    return idfm_credentials_dict["global_app"]["id"], idfm_credentials_dict["global_app"]["key"]


def load_idfm_journey_planner_app_credentials(idfm_credentials_path=DEFAULT_IDFM_CREDENTIALS_PATH):
    idfm_credentials_dict = load_idfm_credentials(idfm_credentials_path=DEFAULT_IDFM_CREDENTIALS_PATH)
    return idfm_credentials_dict["journey_planner_app"]["id"], idfm_credentials_dict["journey_planner_app"]["key"]


def get_idfm_api_token(id_str, key_str):
    '''
    https://portal.api.iledefrance-mobilites.fr/fr/help-center-fr/discussions/recuperation-du-token-oauth2

    L'utilisation des API IDFM nécessite de récupérer un token (protocole OAuth2) depuis un serveur spécifique.

    L’URL de ce serveur d’authentification est : https://as.api.iledefrance-mobilites.fr/api/oauth/token

    La durée du token est de 1h.

    Exemple d'appel (en Python) :

    ```
    A faire au moins une fois par heure
    -----------------------------------

    urlOAuth = 'https://as.api.iledefrance-mobilites.fr/api/oauth/token'
    client_id='',
    client_secret=''

    #Paramètres de la requête de demande de token
    data = dict(
    grant_type='client_credentials',
    scope='read-data',
    client_id=client_id,
    client_secret=client_secret
    )

    response = requests.post(urlOAuth, data=data)
    print(response.json)

    if response.status_code != 200:
    print('Status:', response.status_code, 'Erreur sur la requête; fin de programme')
    exit()

    jsonData = response.json()
    token = jsonData['access_token']


    A faire pour chaque requête globale
    -----------------------------------

    #Paramètres de la requête (URL?)
    url = 'https://traffic.api.iledefrance-mobilites.fr/v1/tr-global/estimated-timetable'

    params = dict(
    LineRef='ALL'
    )
    headers = {
    'Accept-Encoding' : 'gzip',
    'Authorization' : 'Bearer ' + token
    }

    response = requests.get(url, params=params, headers=headers)

    if response.status_code != 200:
    print('Status:', response.status_code, 'Erreur sur la requête; fin de programme')
    exit()

    jsonData = response.json()
    ```
    '''
    
    url_oauth = 'https://as.api.iledefrance-mobilites.fr/api/oauth/token'

    client_id = id_str
    client_secret = key_str

    # Paramètres de la requête de demande de token
    data = {
        'grant_type': 'client_credentials',
        'scope': 'read-data',
        'client_id': client_id,
        'client_secret': client_secret
    }

    response = requests.post(url_oauth, data=data)
    #print(response.json)

    if response.status_code != 200:
        raise Exception('Status:', response.status_code, 'Erreur sur la requête; fin de programme')

    response_dict = response.json()
    token = response_dict['access_token']

    return response_dict["access_token"]


def get_idfm_unitary_app_api_token(idfm_credentials_path=DEFAULT_IDFM_CREDENTIALS_PATH):
    id_str, key_str = load_idfm_unitary_app_credentials(idfm_credentials_path)
    api_token = get_idfm_api_token(id_str, key_str)
    return api_token


def get_idfm_global_app_api_token(idfm_credentials_path=DEFAULT_IDFM_CREDENTIALS_PATH):
    id_str, key_str = load_idfm_global_app_credentials(idfm_credentials_path)
    api_token = get_idfm_api_token(id_str, key_str)
    return api_token


def get_idfm_journey_planner_app_api_token(idfm_credentials_path=DEFAULT_IDFM_CREDENTIALS_PATH):
    id_str, key_str = load_idfm_journey_planner_app_credentials(idfm_credentials_path)
    api_token = get_idfm_api_token(id_str, key_str)
    return api_token
